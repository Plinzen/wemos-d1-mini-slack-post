#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include "config.h"

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;
const String hookUrl = SLACK_HOOK_URL;

const char* host = "hooks.slack.com";
const int httpsPort = 443;

// Use web browser to view and copy
// SHA1 fingerprint of the certificate
const char* fingerprint = "35 85 74 EF 67 35 A7 CE 40 69 50 F3 C0 F6 80 CF 80 3B 2E 19";

const int inputPin = D3;

WiFiClientSecure client;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);

  pinMode(BUILTIN_LED, OUTPUT); 
  pinMode(inputPin, INPUT);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

}

void loop() {
  int buttonTrigger = digitalRead(inputPin);  
  Serial.printf("buttonTrigger is %d\n", buttonTrigger);
  digitalWrite(BUILTIN_LED, buttonTrigger);
  boolean connectedToWifi = WiFi.status() == WL_CONNECTED;
  Serial.printf("Connected to WiFi %d\n", connectedToWifi);
  if (connectedToWifi && buttonTrigger == 0) {
    Serial.println("Connected to WiFi and Button triggered");
    Serial.println("");
    Serial.println("Local IP address: ");
    Serial.println(WiFi.localIP());
     // Use WiFiClientSecure class to create TLS connection
    Serial.print("connecting to ");
    Serial.println(host);
    postSlackMessage();    
    Serial.println("Close");
  }
  delay(100);
}

void postSlackMessage() {
  WiFiClientSecure client;
  int connectionState = client.connect(host, httpsPort);
  if (connectionState) {
    Serial.println("Connection successful");
    client.println("POST " + hookUrl + " HTTP/1.1");
    client.println("Content-Type: application/json");
    client.println("cache-control: no-cache");
    client.println("User-Agent: WemosD1Mini");
    client.println("Accept: */*");
    client.println("Host: hooks.slack.com");
    client.println("content-length: 101");
    client.println("Connection: close");
    client.println("");
    client.println("{\"text\": \"Fresh coffee is available!\", \"channel\": \"#freshly_brewed_coffee\", \"username\": \"coffee-bot\"}");
    delay(10);
    String response = client.readString();    
    Serial.println("Response: " + response);
  } else {
    Serial.printf("Connection failed with error code: %d\n", connectionState);
  }
  client.stop();
}
